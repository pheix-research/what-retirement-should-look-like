#!/usr/bin/env raku

use Test;
use JSON::Fast;
use Crypt::LibGcrypt::Random:ver<1.0.9+>;
use Net::Ethereum;
use Bitcoin::Core::Secp256k1;
use Node::Ethereum::Keccak256::Native;
use Node::Ethereum::KeyStore::V3:ver<0.0.23+>;
use WRSLL::Notify;
use WRSLL::Balance;

constant HEARTBEAT  = 1000;
constant pwd        = 'GimmmeEth';
constant ks         = Node::Ethereum::KeyStore::V3.new;

constant alchemy   = %*ENV<ALCHEMYKEY> // q{};
constant user      = %*ENV<MATRIXUSER> // q{};
constant password  = %*ENV<MATRIXPASS> // q{};
constant roomid    = %*ENV<MATRIXROOM> // q{};
constant secp256k1 = Bitcoin::Core::Secp256k1.new;
constant keccak    = Node::Ethereum::Keccak256::Native.new;

sub MAIN (Str :$address = '0x224b48cafcb5d1c73e4e4494cf546dd3c37f1019') {
    X::AdHoc.new(:payload('no valid Ethereum address is given')).throw unless $address ~~ /^ '0x'<xdigit> ** 40 $/;

    my $eth = Net::Ethereum.new(:api_url(sprintf("https://eth-mainnet.g.alchemy.com/v2/%s", alchemy)));
    my $bal = WRSLL::Balance.new;

    my UInt $index = 1;

    sprintf("[%s %d] grinding %s (notification to %s:%s)...", $bal.get_date, $*PID, $address.lc, user, roomid).say;

    react {
        whenever signal(SIGTRAP) -> $sig {
            diag(sprintf("%s trace dump:\n%s", $sig, to-json($bal.trace.tail, :sorted-keys)));
            sprintf("%s/grinder-%s-trace.json", $*HOME, $bal.get_date).IO.spurt(to-json($bal.trace.reverse, :sorted-keys));
        };
        whenever signal(SIGTERM,SIGINT,SIGQUIT,SIGHUP) -> $sig {
            diag(sprintf("Grinder is forced to finish via %s signal", $sig));
            exit;
        };
        whenever Supply.from-list(1..∞, scheduler => $*SCHEDULER) -> $iteration {
            $bal.addtrace(DateTime.now, $iteration);

            my buf8 $private_key = $bal.addtrace(random(buf8.allocate(32, 0), :very-strong), $iteration);
            (my Str $secret      = $bal.addtrace($eth.buf2hex($private_key), $iteration)) ~~ s/^ '0x' //;
            my $pubkey           = $bal.addtrace(secp256k1.compressed_public_key(:pubkey(secp256k1.create_public_key(:privkey($secret))), :cmp(False)), $iteration);

            X::AdHoc.new(:payload('public key length')).throw unless $pubkey && $pubkey.bytes == 65;

            my $addr  = $eth.buf2hex(keccak.keccak256(:msg($pubkey.subbuf(1, *))).subbuf(*-20));
            my $debug = {
                secret                => $secret,
                public_key            => $eth.buf2hex(buf8.new(secp256k1.key)).lc,
                compressed_public_key => $eth.buf2hex($pubkey).lc,
                keccak                => $eth.buf2hex(keccak.keccak256(:msg($pubkey.subbuf(1, *)))).lc,
            };

            $bal.addtrace($debug, $iteration);

            if $addr.lc eq $address.lc {
                sprintf("[%s] %09d: %s is grinded! Private key: %s", $bal.get_date, $index, $addr, $secret).say;

                my $path     = sprintf("%s/%s.json", $*HOME, $bal.get_filename(:address($addr)));
                my $keystore = $bal.addtrace(ks.keystore(:password(pwd), :$secret, :debug(True)), $iteration);

                ks.save(:$keystore, :$path, :overwrite(True));

                my $payload = $keystore;

                $payload<grinder><balance> = $bal.get_balance(:$eth, :address($keystore<address>), :stats({index => $index}));
                $payload<grinder><secret>  = $secret;
                $payload<grinder><address> = $keystore<address>;
                $payload<grinder><debug>   = $debug;

                my $topic = sprintf("grinder.%s", $*KERNEL.hostname);

                WRSLL::Notify
                    .new(:user(user), :password(password), :roomid(roomid))
                    .send_notification(:$topic, :payload($keystore));

                exit;
            }
            else {
                sprintf("[%s %d] processed %09d iterations", $bal.get_date, $*PID, $index).say if $index % HEARTBEAT == 0;
            }

            $index++;
        }
    };

    return 0;
}
