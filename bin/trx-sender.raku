#!/bin/env raku

use JSON::Fast;
use Pheix::Model::Database::Access;
use Node::Ethereum::Keccak256::Native;
use Pheix::Model::Database::Blockchain::SendTx;
use Node::Ethereum::KeyStore::V3;
use Ethelia::Explorer;

constant address = '0x3f17f1962b36e491b30a40b2405849e597ba5fb5';

my $jsonobj;
my $remotetab;
my $datatab = 'ExtensionEthelia';

sub MAIN (
    Str :$table = 'ExtensionEthelia',
    Str :$storageconfig = sprintf("%s/git/dcms-raku/conf", $*HOME),
    Str :$remotetableprefix = 'remote',
    Str :$abi = sprintf("%s/git/dcms-raku/conf/system/eth/PheixDatabase.abi", $*HOME),
) {
    die sprintf("abi file %s is not found", $abi) unless $abi.IO.f;

    my $start      = now;
    my $trxdecoder = Ethelia::Explorer.new(:$abi);

    $datatab = sprintf("%s/%s", $remotetableprefix, $table);
    $jsonobj = Pheix::Model::JSON.new(:path($storageconfig));

    my %tabsets = $jsonobj.get_all_settings_for_group_member('Pheix', 'storage', $datatab);

    die sprintf("no table %s is found", $datatab) unless %tabsets && %tabsets.keys;

    sprintf("found table %s in Pheix settings (type=%d, hash=%s)", $table, %tabsets<type>, %tabsets<hash>).say;

    my $dbobj = Pheix::Model::Database::Access.new(
        :table($datatab),
        :fields(List.new),
        :jsonobj($jsonobj)
    );

    die 'blockchain database object failure' unless $dbobj && $dbobj.dbswitch == 1;

    my $ethobj = $dbobj.chainobj.ethobj;

    $ethobj.keepalive = False;
    $ethobj.abi       = $trxdecoder.abi.IO.slurp;

#    my $account = $dbobj.chainobj.ethacc;
    my $account = address;
    my $nonce   = $ethobj.eth_getTransactionCount(:data($account), :tag('latest'));

    my $signer = Pheix::Model::Database::Blockchain::SendTx
        .new(
            :signerobj($dbobj.chainobj.sgnobj),
            :targetobj($dbobj.chainobj),
            :debug(True),
            :diag($dbobj.chainobj.diag)
        );

    die 'no keystore password' unless $signer.signerobj.ethobj.unlockpwd;

    my %trx =
        from  => $account,
        to    => $account,
        nonce => $nonce,
        value => 10000000000000000;

    my $esimated_gas = $ethobj.eth_estimateGas(%trx);
    my $fees         = $ethobj.get_fee_data;

    %trx<gas>                  = ($esimated_gas * $signer.gasprmult).Int;
    %trx<maxfeepergas>         = $fees<maxFeePerGas>;
    %trx<maxpriorityfeepergas> = $fees<maxPriorityFeePerGas>;

    %trx.gist.say;

    my $privatekey = {
        str  => $ethobj.buf2hex(buf8.allocate(32, 0)),
        buf8 => buf8.allocate(32, 0),
    };

#    my $privatekey = Node::Ethereum::KeyStore::V3
#        .new(:keystorepath($signer.signerobj.config<keystore>))
#        .decrypt_key(:password($signer.signerobj.ethobj.unlockpwd));

    $privatekey<str> ~~ s/0x//;

    $privatekey.gist.say;

    my %sign = $signer.sign_transaction(
        :trx(%trx),
        :$privatekey
    );

    die 'sign transaction failure' unless %sign<raw> ~~ m:i/^ 0x<xdigit>+ $/;

    to-json(%sign).say;

    my $updatedtrx = $ethobj.eth_sendRawTransaction(:data(%sign<raw>));

    sprintf("processed trx nonce=%d: %s", %trx<nonce>, $updatedtrx).say;

    die 'transaction is not mined' unless $dbobj.chainobj.wait_for_transactions(:hashes([$updatedtrx]));

    sprintf("advancing is done in %d seconds", (now - $start).Int).say;
}
