#!/usr/bin/env raku

use Test;
use JSON::Fast;
use Crypt::LibGcrypt::Random:ver<1.0.9+>;
use Net::Ethereum;
use Node::Ethereum::KeyStore::V3:ver<0.0.23+>;
use WRSLL::Notify;
use WRSLL::Balance;

constant HEARTBEAT  = 100;
constant pwd        = 'EthHunter';
constant ks         = Node::Ethereum::KeyStore::V3.new;
constant mintrigger = 0;

constant alchemy  = %*ENV<ALCHEMYKEY> // q{};
constant user     = %*ENV<MATRIXUSER> // q{};
constant password = %*ENV<MATRIXPASS> // q{};
constant roomid   = %*ENV<MATRIXROOM> // q{};

sub MAIN {
    my $eth = Net::Ethereum.new(:api_url(sprintf("https://eth-mainnet.g.alchemy.com/v2/%s", alchemy)));
    my $bal = WRSLL::Balance.new;

    my $stats = {
        started => now.UInt,
        index   => 1,
        permin  => 0,
        perhour => 0,
        perday  => 0,
    };

    my Bool $printstat = False;
    my Str  $logpath   = sprintf("%s/%s-stats.log", $*HOME, $bal.get_date);

    sprintf("[%s %d] crawling with notifications to %s:%s...", $bal.get_date, $*PID, user, roomid).say;

    react {
        whenever signal(SIGTRAP) -> $sig {
            diag(sprintf("%s trace dump:\n%s", $sig, to-json($bal.trace.tail, :sorted-keys)));
            sprintf("%s/crawler-%s-trace.json", $*HOME, $bal.get_date).IO.spurt(to-json($bal.trace.reverse, :sorted-keys));
        }
        whenever signal(SIGTERM,SIGINT,SIGQUIT,SIGHUP) -> $sig {
            diag(sprintf("Crawler is forced to finish via %s signal", $sig));
            exit;
        }
        whenever Supply.from-list(1..∞, scheduler => $*SCHEDULER) -> $iteration {
            $bal.addtrace(DateTime.now, $iteration);

            my buf8 $private_key = $bal.addtrace(random(buf8.allocate(32, 0), :very-strong), $iteration);
            (my Str $secret      = $bal.addtrace($eth.buf2hex($private_key), $iteration)) ~~ s/^ '0x' //;
            my $keystore         = $bal.addtrace(ks.keystore(:password(pwd), :$secret, :debug(True)), $iteration);

            # $keystore<address> = '0x4fd7997Ec480d01cA833B9F99116f079739C7115';

            my $balance = $bal.addtrace($bal.get_balance(:$eth, :address($keystore<address>), :$stats), $iteration);

            if $balance > 0 {
                sprintf("[%s %d] %09d: found %s ethers on %s balance! Private key: %s", $bal.get_date, $*PID, $stats<index>, ~$balance, $keystore<address>, $secret).say;

                my $path = sprintf("%s/%s.json", $*HOME, $bal.get_filename(:address($keystore<address>)));
                ks.save(:$keystore, :$path, :overwrite(True));

                my $payload = $keystore;

                $payload<crawler><balance> = $balance;
                $payload<crawler><secret>  = $secret;
                $payload<crawler><address> = $keystore<address>;

                my $topic = sprintf("crawler.%s", $*KERNEL.hostname);

                WRSLL::Notify
                    .new(:user(user), :password(password), :roomid(roomid))
                    .send_notification(:$topic, :$payload);

                exit;
            }
            else {
                my $minuntes = (now.UInt - $stats<started>) / 60;

                $stats<permin>  = ($stats<index> / $minuntes);
                $stats<perhour> = ($stats<index> / $minuntes * 60);
                $stats<perday>  = ($stats<index> / $minuntes * 60 * 24);

                sprintf("[%s %d] %09d: processed %s with %s balance (%.01f req/min)", $bal.get_date, $*PID, $stats<index>, $keystore<address>, ~$balance, $stats<permin>).say if ($stats<index> % HEARTBEAT == 0);

                $bal.addtrace($stats, $iteration);

                if my $datetime = DateTime.now(:timezone($*TZ)) {
                    if $datetime.minute == mintrigger && !$printstat {
                        my $msg = $bal.get_stats(:$stats);

                        diag($msg);

                        if $datetime.hour == 0 {
                            $msg ~~ s:g/<[\n]>+/ /;

                            if $logpath.IO.f {
                                $logpath.IO.spurt(sprintf("%s\n", $msg), :append);
                            }
                            else {
                                $logpath.IO.spurt(sprintf("%s\n", $msg));
                            }
                        }

                        $printstat = True;
                    }
                    else {
                        $printstat = False if $datetime.minute != mintrigger;
                    }
                }
            }

            $stats<index> = $iteration;
        }
    }

    return 0;
}
