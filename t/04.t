use v6.d;
use Test;

use JSON::Fast;
use Net::Ethereum;
use Bitcoin::Core::Secp256k1;
use Node::Ethereum::Keccak256::Native;

constant skiptest  = %*ENV<WRSLLTEST> ?? False !! True;
constant eth       = Net::Ethereum.new;
constant secp256k1 = Bitcoin::Core::Secp256k1.new;
constant keccak    = Node::Ethereum::Keccak256::Native.new;
constant address   = '0x3f17f1962b36e491b30a40b2405849e597ba5fb5';

plan 1;

if skiptest {
    skip-rest('No WRSLLTEST env variable is set. Do not set it up until patched libsecp256k1 is installed!');
    exit;
}
else {
    subtest {
        my $blank_pubk = buf8.allocate(65, 0);
        (my $privkey   = eth.buf2hex(buf8.allocate(32, 0))) ~~ s/0x//;

        diag($privkey);

        ok secp256k1.verify_private_key(:privkey($privkey)),  'privkey verified';

        my $pubkey    = secp256k1.create_public_key(:$privkey);
        my $cmppubkey = secp256k1.compressed_public_key(:$pubkey, :cmp(False));

        diag(eth.buf2hex($pubkey).lc);
        diag(eth.buf2hex($cmppubkey).lc);

        is eth.buf2hex(keccak.keccak256(:msg($cmppubkey.subbuf(1, *))).subbuf(*-20)).lc, address, sprintf("address %s", address);
    }, 'key surgery';
}

done-testing;
