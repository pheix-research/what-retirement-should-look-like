use v6.d;
use Test;

constant btc_history = './t/data/btc-history.csv';
constant tailshift   = 10;

plan 1;

subtest {
    plan 2;

    my $history;

    btc_history.IO.lines.map({
        my ($key, $value) = $_.chomp.split(q{,}, :skip_emply);
        $history{$key} = :16($value).Int;
    });

    ok $history.keys, 'history keys';
    ok $history.values, 'history values';

    diag($history.keys.sort[*-tailshift..*].join(','));
    diag($history.keys.sort[*-tailshift..*].map({$history{$_}}).join(','));
}, 'parse BTC history';

done-testing;
