use v6.d;
use Test;

use JSON::Fast;
use Net::Ethereum;
use Node::Ethereum::KeyStore::V3:ver<0.0.23+>;
use Crypt::LibGcrypt::Random:ver<1.0.5+>;
use Bitcoin::Core::Secp256k1;
use Node::Ethereum::Keccak256::Native;

constant infinite  = %*ENV<WRSLLTEST> ?? False !! True;
constant address   = @*ARGS[0] // q{};
constant withks    = @*ARGS[1] ?? True !! False;
constant secp256k1 = Bitcoin::Core::Secp256k1.new;
constant keccak    = Node::Ethereum::Keccak256::Native.new;

plan 1;

if address !~~ m:i/^ 0x<xdigit>**40 $/ {
    skip-rest('no valid input address');
    exit;
}

my $index = 1;
my $eth   = Net::Ethereum.new;
my $ksobj = Node::Ethereum::KeyStore::V3.new;

diag(sprintf("testing %s, use keystore: %s, run infinite: %s", address, ~withks, ~infinite));

for (^∞).values -> $index {
    my $address;
    my $debug;

    my buf8 $private_key = random(buf8.allocate(32, 0), :very-strong);
    (my Str $secret      = $eth.buf2hex($private_key)) ~~ s/^ '0x' //;

    if withks {
        my $keystore = $ksobj.keystore(:password('EthHunter'), :$secret, :debug(True));

        $address = $keystore<address>;
        $debug   = $keystore<debug> // {};
    }
    else {
        my $pubkey = secp256k1.compressed_public_key(:pubkey(secp256k1.create_public_key(:privkey($secret))), :cmp(False));

        X::AdHoc.new(:payload('public key length')).throw unless $pubkey && $pubkey.bytes == 65;

        $address = $eth.buf2hex(keccak.keccak256(:msg($pubkey.subbuf(1, *))).subbuf(*-20));
        $debug   = {
            secret                => $secret,
            public_key            => $eth.buf2hex(buf8.new(secp256k1.key)).lc,
            compressed_public_key => $eth.buf2hex($pubkey).lc,
            keccak                => $eth.buf2hex(keccak.keccak256(:msg($pubkey.subbuf(1, *)))).lc,
        };
    }

    if $address.lc eq address.lc {
        diag(to-json($debug));
        diag($address.lc);

        last;
    }

    if $index && $index % 1000 == 0 {
        diag(sprintf("%012d: %s", $index, $address.lc));

        last if !infinite;
    };
}

ok 1, sprintf("%s is found", address);

done-testing;
