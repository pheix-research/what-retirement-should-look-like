use v6.d;
use Test;

use JSON::Fast;
use Net::Ethereum;
use Bitcoin::Core::Secp256k1;
use Node::Ethereum::Keccak256::Native;

constant eth       = Net::Ethereum.new;
constant secp256k1 = Bitcoin::Core::Secp256k1.new;
constant keccak    = Node::Ethereum::Keccak256::Native.new;
constant offset    = @*ARGS[0] && @*ARGS[0] ~~ /^<[0..9]>+$/ ?? @*ARGS[0] !! 2;
constant silent    = %*ENV<WRSLLTEST> ?? True !! False;
constant pattern   = '1f';
constant address   = '0x3f17f1962b36e491b30a40b2405849e597ba5fb5';

plan 3;

ok offset, sprintf("offset is %d", offset);

subtest {
    my buf8 $private_key = buf8.allocate((32 - offset), 0);
    (my Str $secret      = eth.buf2hex($private_key)) ~~ s/^ '0x' //;

    ok $private_key.bytes, sprintf("secret bulk %d bytes", $private_key.bytes);

    for ^offset -> $value {
        my @subst = "%x%x" xx offset;
        @subst[$value] = pattern;

        my $pattern = sprintf(("%s" xx (offset + 1)).join, $secret, @subst);

        bruteforce(:$pattern, :arr(Array.new), :dimension((offset * 2) - 2));
    }

    ok 1, 'I will guide the blind in darkness though I cannot see myself';
}, 'diary of dreams';

subtest {
    my $postmortem = from-json(
q~{
   "public_key": "0xc8c16c3e19f553e1794428dcdee5cb7f2ac260ac3f073fae4a9879993afb4086961a8ba76b599abf5635230c64ca60e167713a2f3bc5871e8327a605c3e0843a",
   "privkey": "00000000000000000000000000000000000000000000000000000000001f0422",
   "compressed_public_key": "0x0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000",
   "public_key_inner": "0xc8c16c3e19f553e1794428dcdee5cb7f2ac260ac3f073fae4a9879993afb4086961a8ba76b599abf5635230c64ca60e167713a2f3bc5871e8327a605c3e0843a",
   "keccak": "0xad3228b676f7d3cd4284a5443f17f1962b36e491b30a40b2405849e597ba5fb5"
}~
    );

    my $blank_pubk = buf8.allocate(65, 0);
    $blank_pubk[0] = 0x4;

    my $pubkeygen = secp256k1.create_public_key(:privkey($postmortem<privkey>));
    my $pubkey    = eth.hex2buf($postmortem<public_key>);
    my $cmppubkey = secp256k1.compressed_public_key(:$pubkey, :cmp(False));
    my $address   = eth.buf2hex(keccak.keccak256(:msg($blank_pubk.subbuf(1, *))).subbuf(*-20));

    diag(eth.buf2hex($pubkeygen).lc);
    diag($postmortem<public_key>);
    diag(eth.buf2hex($cmppubkey).lc);
    diag($postmortem<compressed_public_key>);
    diag(eth.buf2hex($blank_pubk));
    diag($address.lc);

    is eth.buf2hex(keccak.keccak256(:msg(eth.hex2buf($postmortem<compressed_public_key>).subbuf(1, *))).subbuf(*-20)).lc, address, sprintf("address1 %s", address);
    is eth.buf2hex(keccak.keccak256(:msg($blank_pubk.subbuf(1, *))).subbuf(*-20)).lc, address, sprintf("address2 %s", address);

}, 'key surgery';

done-testing;

sub bruteforce(Str :$pattern, :@arr, UInt :$dimension) {
    if $dimension == 0 {
        my $privkey = sprintf($pattern, @arr);

        ok secp256k1.verify_private_key(:$privkey), sprintf("%s", $privkey) unless silent;

        my $pubkey    = secp256k1.create_public_key(:$privkey);
        my $cmppubkey = secp256k1.compressed_public_key(:$pubkey, :cmp(False));
        my $address   = eth.buf2hex(keccak.keccak256(:msg($cmppubkey.subbuf(1, *))).subbuf(*-20));

        if $address.lc eq address.lc {
            my $debug   = {
                privkey               => $privkey,
                public_key            => eth.buf2hex($pubkey).lc,
                public_key_inner      => eth.buf2hex(buf8.new(secp256k1.key)).lc,
                compressed_public_key => eth.buf2hex($cmppubkey).lc,
                keccak                => eth.buf2hex(keccak.keccak256(:msg($cmppubkey.subbuf(1, *)))).lc,
            };

            diag(to-json($debug));

            die sprintf("%s will whisper %s in a deaf ear while I know you cannot speak", $address.lc, $privkey.lc);
        }
    }
    else {
        my @array = @arr;
        for ^16 -> $digit {
            @array[$dimension - 1] = $digit;
            bruteforce(:$pattern, :arr(@array), :dimension($dimension - 1));
        }
    }
}
