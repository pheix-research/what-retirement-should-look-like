#!/bin/bash

RESTARTS=365
THRESHOLD=86400
COUNTERITERS=0
COUNTERRESTARTS=0
CRAWLERPID=
RESTART_TIMEOUT=30

while [ "$COUNTERRESTARTS" -lt "$RESTARTS" ]; do
        if [ "$COUNTERITERS" -eq "0" ]; then
                ALCHEMYKEY=**** \
                MATRIXUSER=@trappist-one:matrix.org \
                MATRIXPASS=**** \
                MATRIXROOM=**** \
                RAKULIB=$RAKULIB,`pwd`,$HOME/git/raku-ethelia/,$HOME/git/pheix-pool/core-perl6/,$HOME/git/pheix-research/raku-dossier-prototype/ \
                        ./bin/crawler.raku & CRAWLERPID=$!

                COUNTERITERS=1
        elif [ "$COUNTERITERS" -eq "$THRESHOLD" ]; then
                kill -SIGTRAP $CRAWLERPID
                sleep $RESTART_TIMEOUT
                kill -SIGTERM $CRAWLERPID

                COUNTERITERS=0
                COUNTERRESTARTS=$((COUNTERRESTARTS+1))

                sleep $RESTART_TIMEOUT
                
                echo "**[INFO] Crawler is restarted for ${COUNTERRESTARTS} time(s)"
        else
                COUNTERITERS=$((COUNTERITERS+1))
        fi

        sleep 1
done

echo "**[INFO] crawler is finished with dignity: restarted ${COUNTERRESTARTS} time(s)"
