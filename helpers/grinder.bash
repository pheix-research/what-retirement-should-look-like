#!/bin/bash

ADDRESS="$1"

if [ -z "${ADDRESS}" ]; then
        ADDRESS="0x0x"
fi

ALCHEMYKEY=**** \
MATRIXUSER=@trappist-one:matrix.org \
MATRIXPASS=**** \
MATRIXROOM=**** \
RAKULIB=$RAKULIB,`pwd`,$HOME/git/raku-ethelia/,$HOME/git/pheix-pool/core-perl6/,$HOME/git/pheix-research/raku-dossier-prototype/ \
    ./bin/grinder.raku --address="${ADDRESS}"
