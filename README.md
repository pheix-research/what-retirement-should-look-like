# What retirement should look like

## About

Ethereum hacker suite.

## Commit transaction

```bash
raku -I$HOME/git/raku-ethelia/lib/ -I$HOME/git/dcms-raku/lib/ bin/trx-sender.raku --storageconfig=$HOME/git/pheix-io/ethelia/sources/bishkek-smog/iqair/custom-conf/ExtensionEthelia --remotetableprefix=alchemy
```

## Approach to boost address scanning

We know **Tether** smart contract address: https://etherscan.io/address/0xdac17f958d2ee523a2206206994597c13d831ec7. So the idea: when someone transfers USDT ERC-20 token, a fee is paid to **Tether** smart contract address, so we can scan smart contract address for transactions to target addresses, that are automatically generated in `crawler`.

So, for now we generate address and then we check its balance. But we can generate a batch of addresses and request a logs from Geth node where `to` (or `from`) address in `topics` will has a filtering [condition](https://docs.alchemy.com/docs/deep-dive-into-eth_getlogs#making-a-request-to-eth_getlogs).

**Tether** smart contract [input data](https://etherscan.io/tx/0x905d3f9846d9cc1c5ed18a6daf1572079542d9ac77de86235eadc3294cee3685):

| #	| Name   | Type     | Data |
|---|--------|----------|------|
| 0	| _to	 | address	| 0x141ccFb62A33ad2F7899ef5aD6d818BAC0611e2c |
| 1	| _value | uint256  | 484800000 |

So it's possible to filter logs by `to` (recipient) topic:

```javascript
{
  "jsonrpc": "2.0",
  "id": 0,
  "method": "eth_getLogs",
  "params": [
    {
      "fromBlock": "earliest",
      "toBlock": "pending",
      "address": "0xdac17f958d2ee523a2206206994597c13d831ec7",
      "topics": [
          "0xddf252ad1be2c89b69c2b068fc378daa952ba7f163c4a11628f55a4df523b3ef", // event signature
          null, // sender
          [
              [
                  "0x000000000000000000000000b983129b6dab1019351ffb9ab8facfeb16aa6f3d",
                  "0x00000000000000000000000046012396cf9fa7c61f04916812e004a4240da651"
              ]
          ], // recipient
      ]
    }
  ]
}
```

Example above will find transfer transactions for addresses `0xb983129b6dab1019351ffb9ab8facfeb16aa6f3` and `0x46012396cf9fa7c61f04916812e004a4240da651` and it means — those addresses are active and possibly have some funds on balance.

The advantage of this approach: we check a few addresses by one call to remote endpoint, but of course block search range should be not so huge (maybe 1000 blocks in the past or so) and that gives a big limitation on address check precision.

### Test on Mainnet

Unfortunately I can not attach to remote Geth console on **Alchemy** remote server and get **400 Bad Request** error:

```bash
nice -n 20 geth attach https://eth-sepolia.g.alchemy.com/v2/VYE1YL...zZWgZ
```

```javascript
{
  "jsonrpc": "2.0",
  "id": 1,
  "error": {
    "code": -32600,
    "message": "Unsupported method: rpc_modules. See available methods at https://docs.alchemy.com/alchemy/documentation/apis"
  }
}
```

#### Manual workaround

Let's check the transaction: https://etherscan.io/tx/0x905d3f9846d9cc1c5ed18a6daf1572079542d9ac77de86235eadc3294cee3685

```bash
#!/bin/bash

METHOD=eth_getTransactionReceipt
PARAMS='"0x905d3f9846d9cc1c5ed18a6daf1572079542d9ac77de86235eadc3294cee3685"'

curl https://eth-sepolia.g.alchemy.com/v2/VYE1YL...zZWgZ \
  -X POST \
  -H "Content-Type: application/json" \
  --data '{"method":"'${METHOD}'","params":['${PARAMS}'],"id":1,"jsonrpc":"2.0"}' \
  | jq .
```

Response:

```javascript
{
  "jsonrpc": "2.0",
  "id": 1,
  "result": {
    "transactionHash": "0x905d3f9846d9cc1c5ed18a6daf1572079542d9ac77de86235eadc3294cee3685",
    "blockHash": "0x131db10af5145b98c67d8092585dcec1508143a47843b7dee1f79e2eebd6a799",
    "blockNumber": "0x139f6fc",
    "logsBloom": "0x00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000011000001000000000000000000000000000000000000000000000000008000000000000000000000000000000000000000000000000000200000000000000000000000000000000000000000010000000000000000000000000000000000000000000000000000000000000000000100000000000000000000000000080000000000000000000000000000000000000000000000002000000004000000000000000000000000000000000000000000000000000000000800000000000000000000000000000000000000000010000000000",
    "gasUsed": "0xf6dd",
    "contractAddress": null,
    "cumulativeGasUsed": "0xb823a5",
    "transactionIndex": "0x88",
    "from": "0x72df4d592c44a296ac0205e1464370cce83f42a9",
    "to": "0xdac17f958d2ee523a2206206994597c13d831ec7",
    "type": "0x2",
    "effectiveGasPrice": "0x408511a6",
    "logs": [
      {
        "blockHash": "0x131db10af5145b98c67d8092585dcec1508143a47843b7dee1f79e2eebd6a799",
        "address": "0xdac17f958d2ee523a2206206994597c13d831ec7",
        "logIndex": "0x10f",
        "data": "0x000000000000000000000000000000000000000000000000000000001ce57600",
        "removed": false,
        "topics": [
          "0xddf252ad1be2c89b69c2b068fc378daa952ba7f163c4a11628f55a4df523b3ef",
          "0x00000000000000000000000072df4d592c44a296ac0205e1464370cce83f42a9",
          "0x000000000000000000000000141ccfb62a33ad2f7899ef5ad6d818bac0611e2c"
        ],
        "blockNumber": "0x139f6fc",
        "transactionIndex": "0x88",
        "transactionHash": "0x905d3f9846d9cc1c5ed18a6daf1572079542d9ac77de86235eadc3294cee3685"
      }
    ],
    "status": "0x1"
  }
}
```

In `logs` section we see:

```javascript
"topics": [
  "0xddf252ad1be2c89b69c2b068fc378daa952ba7f163c4a11628f55a4df523b3ef",
  "0x00000000000000000000000072df4d592c44a296ac0205e1464370cce83f42a9",
  "0x000000000000000000000000141ccfb62a33ad2f7899ef5ad6d818bac0611e2c"
],
```

Obviously logs for **Tether** smart contract address contain `from` and `to` addresses (`72df4d592c44a296ac0205e1464370cce83f42a9` and `141ccfb62a33ad2f7899ef5ad6d818bac0611e2c` respectively) and we can search by sender and recipient via single run.

#### Filtering for real cases

Script:

```bash
#!/bin/bash

METHOD=eth_getLogs

curl https://eth-sepolia.g.alchemy.com/v2/VYE1YL...zZWgZ \
  -X POST \
  -H "Content-Type: application/json" \
  --data '{"method":"'${METHOD}'","params":[{"fromBlock": "0x139F6FC", "toBlock": "latest", "address": "0xdac17f958d2ee523a2206206994597c13d831ec7", "topics": ["0xddf252ad1be2c89b69c2b068fc378daa952ba7f163c4a11628f55a4df523b3ef", null, ["0x000000000000000000000000141ccfb62a33ad2f7899ef5ad6d818bac0611e2c", "0x000000000000000000000000C67Cc7C32980dD63B10Bb0d0A73E23F501aAAe88"]]}],"id":1,"jsonrpc":"2.0"}' \
  | jq .
```

It found 2 transactions for `0x141ccfb62a33ad2f7899ef5ad6d818bac0611e2c` and `0xc67cc7c32980dd63b10bb0d0a73e23f501aaae88` recipients:

```javascript
{
  "jsonrpc": "2.0",
  "id": 1,
  "result": [
    {
      "address": "0xdac17f958d2ee523a2206206994597c13d831ec7",
      "blockHash": "0x131db10af5145b98c67d8092585dcec1508143a47843b7dee1f79e2eebd6a799",
      "blockNumber": "0x139f6fc",
      "data": "0x000000000000000000000000000000000000000000000000000000001ce57600",
      "logIndex": "0x10f",
      "removed": false,
      "topics": [
        "0xddf252ad1be2c89b69c2b068fc378daa952ba7f163c4a11628f55a4df523b3ef",
        "0x00000000000000000000000072df4d592c44a296ac0205e1464370cce83f42a9",
        "0x000000000000000000000000141ccfb62a33ad2f7899ef5ad6d818bac0611e2c"
      ],
      "transactionHash": "0x905d3f9846d9cc1c5ed18a6daf1572079542d9ac77de86235eadc3294cee3685",
      "transactionIndex": "0x88"
    },
    {
      "address": "0xdac17f958d2ee523a2206206994597c13d831ec7",
      "topics": [
        "0xddf252ad1be2c89b69c2b068fc378daa952ba7f163c4a11628f55a4df523b3ef",
        "0x0000000000000000000000001d0a26327f5e20f36a171fe726a07107ec2952b4",
        "0x000000000000000000000000c67cc7c32980dd63b10bb0d0a73e23f501aaae88"
      ],
      "data": "0x0000000000000000000000000000000000000000000000000000000002625a00",
      "blockNumber": "0x139f888",
      "transactionHash": "0x5db632e99836b7170da88debb3d40320e35b07aece2a3732486f7b0301fee0ab",
      "transactionIndex": "0x15f",
      "blockHash": "0x312f9de8e752ed06323c44bacc0993ecd6eb84c09f00976216f0ff12ab2af2bd",
      "logIndex": "0x1f6",
      "removed": false
    }
  ]
}
```

##### This optimization approach works perfectly 😎

## Notes

1. [Notes about BTC mining via BasicSwap](https://gitlab.com/pheix-research/what-retirement-should-look-like/-/blob/main/docs/btc-basicswap-bfgminer.md)

## Author

Please contact me via [Matrix](https://matrix.to/#/@k.narkhov:matrix.org) or [LinkedIn](https://www.linkedin.com/in/knarkhov/). Your feedback is welcome at [narkhov.pro](https://narkhov.pro/contact-information.html).
