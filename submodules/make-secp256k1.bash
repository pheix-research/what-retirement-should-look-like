#!/bin/bash

DOCLEAN="$1"
BUILDPATH=`pwd`

cd ${BUILDPATH}/secp256k1

if [ ! -z "${DOCLEAN}" ]; then
	sudo make distclean
fi

./autogen.sh && ./configure --enable-module-recovery && make && sudo make install

if [ $? -ne 0 ]; then
	echo "build failed"
else 
	echo "build ok"
fi

