# Notes about BTC mining via BasicSwap

## Overview

### Exposing `bitcoind` ports

In `/home/kostas/git/basicswap/docker` we have to tweak the next files:

1. Set `BTC_PORT` at environment config file `.env`:

```
HTML_PORT=127.0.0.1:12700:12700
WS_PORT=127.0.0.1:11700:11700
BTC_PORT=127.0.0.1:19996:19996
COINDATA_PATH=/var/data/coinswaps
TOR_PROXY_HOST=172.16.238.200
TZ=Asia/Bishkek
```

2. Expose `BTC_PORT` at generic `docker-composer` configuration file `docker-compose.yml`:

```yaml
version: '3.4'
services:
    swapclient:
        image: i_swapclient
        stop_grace_period: 5m
        build:
            context: ../
        volumes:
            - ${COINDATA_PATH}:/coindata
        ports:
            - "${HTML_PORT}"  # Expose only to localhost, see .env
            - "${WS_PORT}"    # Expose only to localhost, see .env
            - "${BTC_PORT}"   # Exposed bitcoind RPC port
        environment:
            - TZ
        logging:
            driver: "json-file"
            options:
                max-size: "10m"
                max-file: "5"

volumes:
    coindata:
        driver: local
```

### Configuring `botcoind`

BasicSwap `bitcoind` configuration folder: `/coindata/bitcoin/`

```
drwxr-xr-x 1 swap_user swap_user      266 Apr 14 14:35 .
drwxr-xr-x 1 swap_user swap_user      312 Apr 14 14:42 ..
-rw------- 1 swap_user swap_user       75 Apr 14 14:20 .cookie
-rw------- 1 swap_user swap_user        0 Mar 25 01:46 .lock
-rw------- 1 swap_user swap_user       31 Mar 25 01:46 banlist.json
-rw-r--r-- 1 swap_user swap_user      155 Apr 14 14:06 bitcoin.conf
-rw------- 1 swap_user root             3 Apr 14 14:20 bitcoind.pid
drwxr-xr-x 1 swap_user swap_user      634 Apr 14 13:25 blocks
drwx------ 1 swap_user swap_user   133154 Apr 14 14:42 chainstate
-rw------- 1 swap_user swap_user 10931873 Apr 14 14:42 debug.log
-rw------- 1 swap_user swap_user   247985 Apr 14 14:17 fee_estimates.dat
-rw------- 1 swap_user root       9884805 Apr 14 14:17 mempool.dat
-rw------- 1 swap_user swap_user  2599974 Apr 14 14:35 peers.dat
-rw------- 1 swap_user swap_user        4 Apr 14 14:20 settings.json
drwx------ 1 swap_user swap_user       70 Apr 14 14:18 wallet.dat
```

Check `bitcoin.conf` for initial setup:

```
rpcport=19996
printtoconsole=0
daemon=0
wallet=wallet.dat
deprecatedrpc=create_bdb
prune=2000
fallbackfee=0.0002
rpcallowip=0.0.0.0/0
main.rpcbind=0.0.0.0
```

Last 2 lines `rpcallowip` and `main.rpcbind` are added to allow connection outside the docker (from host): https://github.com/SatoshiPortal/dockers/issues/18.

Login and password for `bitcoind` are automatically generated and stored in `.cookie` file:

```
__cookie__:77235f8410851b4e40058bfbc2d0b84c44ca43f824ca9b06b8d38747a1510200
```

## Commands

Bitcoin API reference: https://developer.bitcoin.org/reference/rpc/index.html

Get basic mining info from `bitcoind`:

```bash
curl -u __cookie__:77235f8410851b4e40058bfbc2d0b84c44ca43f824ca9b06b8d38747a1510200 --data-binary '{"jsonrpc": "1.0", "id":"curltest", "method": "getmininginfo", "params": [] }' -H 'Content-Type:application/json' localhost:19996 | jq .
```

```javascript
{
  "result": {
    "blocks": 839175,
    "currentblockweight": 3995956,
    "currentblocktx": 4569,
    "difficulty": 86388558925171.02,
    "networkhashps": 6.878573094005857E+20,
    "pooledtx": 11475,
    "chain": "main",
    "warnings": ""
  },
  "error": null,
  "id": "curltest"
}
```

Get RPC details from `bitcoind`:

```bash
curl -u __cookie__:77235f8410851b4e40058bfbc2d0b84c44ca43f824ca9b06b8d38747a1510200 --data-binary '{"jsonrpc": "1.0", "id":"curltest", "method": "getrpcinfo", "params": [] }' -H 'Content-Type:application/json' localhost:19996 | jq .
```

```javascript
{
  "result": {
    "active_commands": [
      {
        "method": "getrpcinfo",
        "duration": 22
      }
    ],
    "logpath": "/coindata/bitcoin/debug.log"
  },
  "error": null,
  "id": "curltest"
}
```

The `getblocktemplate` RPC gets a block template or proposal for use with mining software:

```bash
curl -u __cookie__:77235f8410851b4e40058bfbc2d0b84c44ca43f824ca9b06b8d38747a1510200 --data-binary '{"id": 0, "method": "getblocktemplate", "params": [{"capabilities": ["coinbasetxn", "coinbasevalue", "workid", "longpoll", "coinbase/append", "time/increment", "version/force", "version/reduce", "submit/coinbase", "submit/truncate"], "maxversion": 1073741823, "rules": ["csv", "segwit"]}]}' -H 'Content-Type:application/json' localhost:19996 | jq .
```

```javascript
{
  "result": {
    "capabilities": [
      "proposal"
    ],
    "version": 536870912,
    "rules": [
      "csv",
      "!segwit",
      "taproot"
    ],
    "vbavailable": {},
    "vbrequired": 0,
    "previousblockhash": "000000000000000000021ca79ba89bf325b552daa15334349f25fdd5510f7101",
    "transactions": [
      {
        "data": "02000000000101627b5d9cdf68701d38cb4111d5dcb6f35e89e8259ada53602ae8d5ef878160190000000000fdffffff02a040dd0000000000160014bd227337642ec818354436d153f4e6d6c248a2cad492160500000000160014ba672c80007173c69a50cdf3023d2cd3e585a49f02473044022060b63daf1403004d676b6b684a37aa94d6e1524da19da2a0a7a1f662a9bf67fd02200aa59569523f6447213cddbb9b151507600a0b8906d3fa03b03ed9bbae6f2d120121022f42d6f1752f4bebc082a0c4d3bec528d9b7fe25975ab7e484d7a4ef17ac0cfe00000000",
        "txid": "bb532ef77188b532bb34470fad313656156c10a08bf04d0298d33503fca4dbb0",
        "hash": "defc991c7e1d273bb730c8064433c5ac2dcb6731e30bf5b2a6bb02fccdafda1f",
        "depends": [],
        "fee": 34540,
        "sigops": 1,
        "weight": 561
      },
      ...
    ],
    "coinbaseaux": {},
    "coinbasevalue": 664357590,
    "longpollid": "000000000000000000021ca79ba89bf325b552daa15334349f25fdd5510f7101327155",
    "target": "0000000000000000000342190000000000000000000000000000000000000000",
    "mintime": 1713111495,
    "mutable": [
      "time",
      "transactions",
      "prevblock"
    ],
    "noncerange": "00000000ffffffff",
    "sigoplimit": 80000,
    "sizelimit": 4000000,
    "weightlimit": 4000000,
    "curtime": 1713113618,
    "bits": "17034219",
    "height": 839218,
    "default_witness_commitment": "6a24aa21a9ed79c62fa92ab00fd487477ae67ce70f3e788a2f3d5627235ff5446e150f12a3d7"
  },
  "error": null,
  "id": 0
}
```

## `bfgminer`

Reference: https://bitcointalk.org/index.php?topic=182276.0

### Build

```bash
CFLAGS="-O2 -msse2" ./configure --enable-cpumining && make
```

### Run

```bash
./bfgminer -S noauto -S cpu:auto -T -D -P -o http://localhost:19996 -u '__cookie__' -p 720861c7982def56a4b448ba22758d361f0b82abc0ca9fce5bfd1e3a4e0b8330 --coinbase-sig "webtech-omen: pheix.org got it!" --generate-to 1CuXnBj49KhjE1NkNScz79qBc4h58oi1ta
```

```
20s:16.19 avg:17.54 u:14.45 Mh/s | A:0 R:0+0(none) HW:0/none
 [2024-04-15 11:43:45] [thread 11: 5654989 hashes, 1414.5 khash/sec]
 [2024-04-15 11:43:45] [thread 3: 5683777 hashes, 1442.2 khash/sec]
 [2024-04-15 11:43:45] [thread 1: 5499783 hashes, 1408.0 khash/sec]
 [2024-04-15 11:43:46] [thread 10: 5397956 hashes, 1378.3 khash/sec]
 [2024-04-15 11:43:46] [thread 2: 5921805 hashes, 1490.2 khash/sec]
 [2024-04-15 11:43:46] [thread 4: 5346815 hashes, 1332.8 khash/sec]
 [2024-04-15 11:43:46] [thread 8: 5620411 hashes, 1407.0 khash/sec]
 [2024-04-15 11:43:47] [thread 5: 5949841 hashes, 1451.1 khash/sec]
 [2024-04-15 11:43:47] [thread 9: 5564814 hashes, 1438.6 khash/sec]
 [2024-04-15 11:43:48] [thread 0: 5398760 hashes, 1340.4 khash/sec]
 [2024-04-15 11:43:48] [thread 7: 64128465 hashes, 1395.6 khash/sec]
 [2024-04-15 11:43:48] [thread 6: 5374922 hashes, 1323.1 khash/sec]
 [2024-04-15 11:43:49] [thread 11: 5659161 hashes, 1461.6 khash/sec]
 [2024-04-15 11:43:49] [thread 1: 5632811 hashes, 1433.0 khash/sec]
 [2024-04-15 11:43:49] [thread 3: 5769787 hashes, 1451.7 khash/sec]
 [2024-04-15 11:43:50] [thread 10: 5514375 hashes, 1418.2 khash/sec]
 [2024-04-15 11:43:50] [thread 2: 5961538 hashes, 1398.0 khash/sec]
 [2024-04-15 11:43:50] [thread 4: 5332162 hashes, 1352.8 khash/sec]
 [2024-04-15 11:43:50] [thread 5: 5805235 hashes, 1494.4 khash/sec]
 [2024-04-15 11:43:51] [thread 8: 5628661 hashes, 1384.0 khash/sec]
 [2024-04-15 11:43:51] [thread 9: 5755459 hashes, 1420.8 khash/sec]
 [2024-04-15 11:43:51] CPU 3 found something?
 [2024-04-15 11:43:51]  Proof: 00000000e8b7482906ff0c718aec3f4e7b3e1f9c51cdfa2a7b97a55461c7828b
Target: 0000000000000000000342190000000000000000000000000000000000000000
TrgVal? no (false positive; hash > target)
 [2024-04-15 11:43:52] [thread 0: 5362494 hashes, 1419.8 khash/sec]
 [2024-04-15 11:43:52] [thread 7: 5582550 hashes, 1453.3 khash/sec]
 [2024-04-15 11:43:52] [thread 6: 5293311 hashes, 1333.8 khash/sec]
 [2024-04-15 11:43:53] [thread 1: 5732678 hashes, 1477.3 khash/sec]
 [2024-04-15 11:43:53] [thread 3: 5807781 hashes, 1414.9 khash/sec]
 [2024-04-15 11:43:53] [thread 11: 5846609 hashes, 1327.1 khash/sec]
 [2024-04-15 11:43:54] [thread 10: 5673293 hashes, 1371.2 khash/sec]
 [2024-04-15 11:43:54] [thread 2: 5592388 hashes, 1497.4 khash/sec]
 [2024-04-15 11:43:54] [thread 4: 5411535 hashes, 1355.7 khash/sec]
 [2024-04-15 11:43:55] [thread 8: 5536545 hashes, 1363.4 khash/sec]
 [2024-04-15 11:43:55] [thread 5: 5977885 hashes, 1438.5 khash/sec]
 [2024-04-15 11:43:55] [thread 9: 5683765 hashes, 1404.4 khash/sec]
 [2024-04-15 11:43:56] [thread 0: 5679815 hashes, 1389.1 khash/sec]
 [2024-04-15 11:43:56] [thread 7: 5813999 hashes, 1382.9 khash/sec]
 [2024-04-15 11:43:56] [thread 6: 5335711 hashes, 1325.5 khash/sec]
 [2024-04-15 11:43:57] [thread 3: 5660470 hashes, 1453.2 khash/sec]
 [2024-04-15 11:43:57] [thread 1: 5910033 hashes, 1417.0 khash/sec]
 [2024-04-15 11:43:57] [thread 11: 5308816 hashes, 1390.5 khash/sec]
 [2024-04-15 11:43:58] [thread 2: 5990261 hashes, 1485.1 khash/sec]
 [2024-04-15 11:43:58] [thread 10: 5485343 hashes, 1298.3 khash/sec]
 [2024-04-15 11:43:59] [thread 4: 5423541 hashes, 1312.8 khash/sec]
 [2024-04-15 11:43:59] [thread 5: 5754909 hashes, 1474.3 khash/sec]
 [2024-04-15 11:43:59] [thread 8: 5454370 hashes, 1343.4 khash/sec]
 [2024-04-15 11:43:59] [thread 9: 5618396 hashes, 1353.8 khash/sec]
 [2024-04-15 11:44:00] [thread 0: 5557336 hashes, 1405.2 khash/sec]
 [2024-04-15 11:44:00] [thread 6: 5302774 hashes, 1389.9 khash/sec]
 [2024-04-15 11:44:00] [thread 7: 5532707 hashes, 1427.1 khash/sec]
 [2024-04-15 11:44:01] [thread 11: 5563133 hashes, 1437.6 khash/sec]
 [2024-04-15 11:44:01] [thread 1: 5668093 hashes, 1436.0 khash/sec]
 [2024-04-15 11:44:01] [thread 3: 5813334 hashes, 1394.7 khash/sec]
 [2024-04-15 11:44:02] [thread 10: 5193435 hashes, 1265.8 khash/sec]
```

## Block historical data

Get block hash historical data via `bitcoin-cli` tool:

```bash
for height in `seq 839320 839336`; do echo "$height `/coindata/bin/bitcoin/bitcoin-cli -datadir=/coindata/bitcoin/ getblockhash $height`"; done
```

```
839320 0000000000000000000260b8c8ec771759a71b8d91a96c111d55018ad1367a59
839321 000000000000000000010517dafba0e22ae4fa01ca345bc2eb9c5c6238ed1f68
839322 000000000000000000025503e7c512d97863fe12c38ef67dda983cc274ef9606
839323 000000000000000000017ac228d39952c5e606b1d74e8be6ef623419b4d69cdf
839324 00000000000000000002dbc97744f94bd89d5c0910eeb914ccbf44ca437147c8
839325 00000000000000000000e8e11a6fabe8f5eef9a1232e7c47def5f8bcf27bb58f
839326 000000000000000000006620b2732bce45052989e0d91bcecf9094882a4cb226
839327 000000000000000000018dff5b10fc39e0783782b3b97dd2a4b3a1a3251201d2
839328 0000000000000000000196ebbbc14b52258310558b3b6db1085de6cf8d21fc6b
839329 00000000000000000002eabea23a1b48ed8efb521ccb083c1e9b1285a8d5d1ad
839330 00000000000000000000ca084882a33d06df93651167322956136038299df6c2
839331 000000000000000000015cd233ff839150cdb474af8bfa4e1f554ea4604debd8
839332 0000000000000000000223def5487b76f9af8d4439bda6a85f1673e2db8ad446
839333 0000000000000000000062bcd198b8b180c772dd28bb75c6117d0b0f8406f980
839334 00000000000000000001d5d569e721fe56fd85ff0ad2b1de1d6f29e1530e688c
839335 00000000000000000000581af7f866e530053928e8b0e7c3dfe899923d64a9fc
839336 000000000000000000018321c995fe89c7dff12de1b86df4b44e01b07e7825aa
```
