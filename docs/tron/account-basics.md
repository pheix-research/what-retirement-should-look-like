# Tron account basics

## Creating Tron accounts

Tron account management is very similar to Ethereum. Let me explain how it works. At first step you need to generate private key:

```bash
raku -MCrypt::LibGcrypt::Random -MNet::Ethereum -e '(my Str $privkey = Net::Ethereum.new.buf2hex(random(32)).lc) ~~ s/^0x//; $privkey.say;'
# a3c17504111937c4abe8b151e49d92a5b05024175dc9f22b1c7248a19648c9f6
```

At second step generate Ethereum-compatible keystore file:

```bash
ethkeystorev3-cli --keystorepath=$HOME/ethereum.keystore --password=SamplePass --privatekey=a3c17504111937c4abe8b151e49d92a5b05024175dc9f22b1c7248a19648c9f6
```

Check the `$HOME/ethereum.keystore` content with `cat $HOME/ethereum.keystore`:

```javascript
{
  "address": "0x41fa54bf2467bb39d50e284307c69c755e011989",
  "version": 3,
  "id": "f68ccdb2-16d0-44b6-9b79-a5f9c82e6b7d",
  "crypto": {
    "cipher": "aes-128-ctr",
    "kdf": "scrypt",
    "ciphertext": "26dd7a10a0182a57d98e413214080ac340937b1adeeae98b297b82a17507cb2d",
    "kdfparams": {
      "dklen": 32,
      "r": 8,
      "p": 1,
      "salt": "6a3e5c5b21f327b92ede250c53426296944ff7be872475517984e5e77bc8c517",
      "n": 262144
    },
    "mac": "7cf55bdc8ef243920b3de619275ff331cc39f7dccf451b6650836f518888d07a",
    "cipherparams": {
      "iv": "9001429a98416f85870168700bba4b21"
    }
  }
}
```

Ethereum address `0x41fa54bf2467bb39d50e284307c69c755e011989` is matched to `a3c17504111937c4abe8b151e49d92a5b05024175dc9f22b1c7248a19648c9f6` private key.

According [official Tron documentation](https://developers.tron.network/docs/account#account-address-format) Tron account in hex format is an Ethereum account where `0x` is replaced to `41`.

So `0x41fa54bf2467bb39d50e284307c69c755e011989` becomes  `4141fa54bf2467bb39d50e284307c69c755e011989`. Finally we need to encode Tron account from hex format to [Base58 format](https://github.com/tronprotocol/documentation/blob/master/TRX/Tron-overview.md#62-mainnet-addresses-begin-with-41) via [online converter](https://tron-converter.com/): `4141fa54bf2467bb39d50e284307c69c755e011989` becomes `TFz4oqJj62FwNqTro57SF7iQSWxJk6Geqv`.

## Check balances

Official endpoint at **tronscan.org**: https://apilist.tronscan.org/api/

```bash
curl https://apilist.tronscan.org/api/account?address=TFz4oqJj62FwNqTro57SF7iQSWxJk6Geqv | jq .balances
```

```javascript
[
  {
    "amount": "0.000000",
    "tokenPriceInTrx": 1,
    "tokenId": "_",
    "balance": "0",
    "tokenName": "trx",
    "tokenDecimal": 6,
    "tokenAbbr": "trx",
    "tokenCanShow": 1,
    "tokenType": "trc10",
    "vip": false,
    "tokenLogo": "https://static.tronscan.org/production/logo/trx.png"
  }
]
```
