unit class WRSLL::Balance;

use Test;
use Net::Ethereum;

has      @.trace;
has UInt $.tracelen = 250;
has UInt $.tries    = 100;
has UInt $.timeout  = 5;

method get_balance(Net::Ethereum :$eth!, Str :$address!, Hash :$stats!) returns Numeric {
    my $balance       = 0;
    my $failure_count = $!tries;

    while ($failure_count > 0) {
        my Bool $failure = False;

        try {
            $balance = $eth.wei2ether($eth.eth_getBalance($address, 'latest'));

            # X::AdHoc.new(:payload(sprintf("emulate failure, tries left: %02d", $failure_count))).throw;

            CATCH {
                default {
                    $failure = True;

                    sprintf("[%s %d] ***ERR on %02d try (iteration=%09d) crawler catch (%s) exception: %s", self.get_date, $*PID, ($!tries - $failure_count + 1), $stats<index>, ~$failure, .message).say;

                    unless --$failure_count {
                        diag(self.get_stats(:$stats, :level('POSTMORTEM')));

                        .throw;
                    }

                    sleep($!timeout);
                }
            }
        }

        last unless $failure;
    }

    return $balance;
}

method get_stats(Hash :$stats = {started => now.UInt, permin => 0, perhour => 0, perday => 0}, Str :$level = 'INFO') returns Str {
    return sprintf(
        "[%s] ***%s stats:\nstarted on %s\n%02d:%02d uptime\n%.01f req/min\n%.01f req/hour\n%.01f req/day",
        self.get_date,
        $level,
        DateTime.new($stats<started>, :timezone($*TZ), :formatter({
            sprintf(
                "%04d-%02d-%02d %02d:%02d:%02u",
                .year,
                .month,
                .day,
                .hour,
                .minute,
                .second.Int,
            )
        })).Str,
        ((now.UInt - $stats<started>) / 3600).floor.UInt,
        (((now.UInt - $stats<started>) % 3600) / 60).floor.UInt,
        $stats<permin>,
        $stats<perhour>,
        $stats<perday>
    );
}

method get_date returns Str {
    return DateTime.now(formatter => {
        sprintf(
            "%04d%02d%02d-%02d:%02d:%02u.%04u",
            .year,
            .month,
            .day,
            .hour,
            .minute,
            .second.Int,
            (.second % (.second.Int || .second)) * 10_000
        )
    }, timezone => $*TZ).Str;
}

method get_filename(Str :$address) returns Str {
    return sprintf("%s-%s", self.get_date, $address);
}

method addtrace($value!, $iteration!) {
    @!trace.shift if @!trace.elems == $!tracelen + 1;

    if @!trace.tail && @!trace.tail<iteration> == $iteration {
        @!trace.tail<steps>{@!trace.tail<steps>.keys + 1} = $value.gist;
    }
    else {
        @!trace.push({
            iteration => $iteration,
            steps => {
                1 => $value.gist;
            }
        })
    }

    return $value;
}
