unit class WRSLL::Notify;

use Pheix::Model::JSON;
use Ethelia;

has Str $.user;
has Str $.password;
has Str $.roomid;
has $.temp_config = {
    module => {
        configuration => {
            settings => {
                defaultnotifier => {
                    value => 'matrix'
                },
                notifier => {
                    group => {
                        matrix => {
                            authendpoint => 'https://matrix.org/_matrix/client/r0/login',
                            notificationendpoint => 'https://matrix.org/_matrix/client/r0/rooms',
                            user => $!user,
                            password => $!password,
                            roomid => $!roomid,
                            notifyontopics => [
                                'crawler',
                                'grinder'
                            ]
                        }
                    }
                }
            }
        }
    }
};

method send_notification(Str :$topic!, :$payload!) returns Bool {
    my $jsonobj = Pheix::Model::JSON.new.set_entire_config(:setup($!temp_config));

    return Ethelia.new(:$jsonobj).send_notification(:$topic, :$payload);
}
